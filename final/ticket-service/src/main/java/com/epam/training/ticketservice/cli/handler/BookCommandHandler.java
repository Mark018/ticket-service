package com.epam.training.ticketservice.cli.handler;

import com.epam.training.ticketservice.cli.UserCommandAvailability;
import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.exception.InvalidSeatException;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.SeatAlreadyTakenException;
import com.epam.training.ticketservice.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

@ShellComponent
public class BookCommandHandler extends UserCommandAvailability {
    @Autowired
    private BookService bookService;

    @ShellMethod(value = "Booking a seat", key = "book")
    @ShellMethodAvailability("isUserHasNotAdmin")
    public String book(String movie, String room, String startdate, String seats) {

        try {
            Book book = bookService.bookSeat(movie, room, startdate, seats);
            return String.format("Seats booked: %s; the price for this booking is %d HUF",
                book.getSeats().toString(", ", "(", ")"), book.getCost());
        } catch (MovieNotFoundException e) {
            return "The specified movie not exists";
        } catch (RoomNotFoundException e) {
            return "The specified room not exists";
        } catch (SeatAlreadyTakenException e) {
            return e.getMessage();
        } catch (InvalidSeatException e) {
            return e.getMessage();
        }
    }
}
