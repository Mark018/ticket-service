package com.epam.training.ticketservice.exception;

public class InvalidSeatException extends Exception {
    public InvalidSeatException(String message) {
        super(message);
    }
}
