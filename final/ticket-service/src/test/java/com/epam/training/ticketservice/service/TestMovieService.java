package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.repository.MovieRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestMovieService {

    @Mock
    private MovieRepository movieRepository;

    @InjectMocks
    private MovieService movieService;

    @Test
    public void testAddMovieShouldAddTheNewMovie() {
        // Given
        Movie movie = new Movie("title", "genre", 1);

        // When
        movieService.addMovie(movie.getTitle(), movie.getGenre(), movie.getDuration());

        // Then
        verify(movieRepository).addMovie(movie);
        Mockito.verifyNoMoreInteractions(movieRepository);
    }

    @Test
    public void testUpdateMovieShouldUpdateTheMovie() throws MovieNotFoundException {
        // Given
        Movie movie = new Movie("title", "genre", 1);

        // When
        movieService.updateMovie(movie.getTitle(), movie.getGenre(), movie.getDuration());

        // Then
        verify(movieRepository).updateMovie(movie.getTitle(), movie.getGenre(), movie.getDuration());
        Mockito.verifyNoMoreInteractions(movieRepository);
    }

    @Test
    public void testDeleteMovieShouldDeleteTheMovie() throws MovieNotFoundException {
        // Given
        Movie movie = new Movie("title", "genre", 1);

        // When
        movieService.deleteMovie(movie.getTitle());

        // Then
        verify(movieRepository).deleteMovie(movie.getTitle());
        Mockito.verifyNoMoreInteractions(movieRepository);
    }

    @Test
    public void testGetMoviesShouldInvokeTheRepositoryGetMovies() {
        // When
        movieService.getMovies();

        // Then
        verify(movieRepository).getMovies();
        Mockito.verifyNoMoreInteractions(movieRepository);
    }
}
