package com.epam.training.ticketservice.converter;

import com.epam.training.ticketservice.dataaccess.converter.Seat;
import com.epam.training.ticketservice.dataaccess.converter.SeatAttributeConverter;
import com.epam.training.ticketservice.dataaccess.converter.SeatList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TestSeatAttributeConverter {
    @InjectMocks
    private SeatAttributeConverter seatAttributeConverter;

    @Test
    public void testConvertToEntityAttributeShouldConvertTheStringToSeatList() {
        // Given
        String dbData = "0,0 1,1 1,2";
        SeatList excepted = new SeatList();
        excepted.add(new Seat(0, 0));
        excepted.add(new Seat(1, 1));
        excepted.add(new Seat(1, 2));

        // Then
        Assertions.assertEquals(excepted, seatAttributeConverter.convertToEntityAttribute(dbData));
    }

    @Test
    public void testConvertToDatabaseColumnShouldConvertTheSeatListToString() {
        // Given
        String excepted = "0,0 1,1 1,2";
        SeatList list = new SeatList();
        list.add(new Seat(0, 0));
        list.add(new Seat(1, 1));
        list.add(new Seat(1, 2));

        // Then
        Assertions.assertEquals(excepted, seatAttributeConverter.convertToDatabaseColumn(list));
    }
}
