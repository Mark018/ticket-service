package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.User;
import com.epam.training.ticketservice.dataaccess.dao.TicketUserDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TestUserService {

    @Mock
    private TicketUserDao ticketUserRepository;

    @InjectMocks
    private TicketUserDetailsService ticketUserDetailsService;

    @Test
    public void testLoadUserByUsernameShouldInvokeTheRepositoryFindByUsername() {
        // Given
        String username = "username";
        User user = new User("test", "test", "test");

        // When
        when(ticketUserRepository.findByUsername(username)).thenReturn(Optional.of(user));
        ticketUserDetailsService.loadUserByUsername(username);

        // Then
        verify(ticketUserRepository).findByUsername(username);
        Mockito.verifyNoMoreInteractions(ticketUserRepository);
    }

    @Test
    public void testLoadUserByUsernameShouldThrowUsernameNotFoundException() {
        // Given
        String username = "username";
        User user = new User("test", "test", "test");

        // When / Then
        Assertions.assertThrows(UsernameNotFoundException.class,
            () -> ticketUserDetailsService.loadUserByUsername(username));
    }

    @Test
    public void testIsUserExistsShouldReturnsFalse() {
        // Given
        String username = "username";

        // When / Then
        Assertions.assertFalse(ticketUserDetailsService.isUserExists(username));
    }

    @Test
    public void testIsUserExistsShouldReturnsTrue() {
        // Given
        String username = "username";
        User user = new User(username, "test", "test");

        // When
        when(ticketUserRepository.findByUsername(username)).thenReturn(Optional.of(user));

        // Then
        Assertions.assertTrue(ticketUserDetailsService.isUserExists(username));
    }

    @Test
    public void testAddUserShouldInvokeTheAddUser() {
        // Given
        String username = "username";
        String password = "password";

        // When
        ticketUserDetailsService.addUser(username, password);

        // Then
        verify(ticketUserRepository).save(any());
    }
}
