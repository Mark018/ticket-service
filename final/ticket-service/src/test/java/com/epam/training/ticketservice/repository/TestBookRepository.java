package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.dataaccess.converter.SeatList;
import com.epam.training.ticketservice.dataaccess.dao.BookDao;
import com.epam.training.ticketservice.dataaccess.dao.MovieDao;
import com.epam.training.ticketservice.dataaccess.dao.RoomDao;
import com.epam.training.ticketservice.dataaccess.dao.TicketUserDao;
import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.dataaccess.model.User;
import com.epam.training.ticketservice.exception.InvalidSeatException;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.SeatAlreadyTakenException;
import com.epam.training.ticketservice.repository.impl.JpaBookRepository;
import com.epam.training.ticketservice.service.TicketUserDetailsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TestBookRepository {
    @InjectMocks
    private JpaBookRepository jpaBookRepository;

    @Mock
    private TicketUserDao ticketUserRepository;

    @Mock
    private BookDao bookDao;

    @Mock
    private MovieDao movieDao;

    @Mock
    private RoomDao roomDao;

    @Test
    public void testGetBooksByUserShouldReturnTheUserBooks() {
        // Given
        String username = "admin";
        User user = Mockito.mock(User.class);
        Book book = Mockito.mock(Book.class);

        // When
        when(ticketUserRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(bookDao.findBooksByUser(user)).thenReturn(Optional.of(Arrays.asList(book)));

        // Then
        Assertions.assertEquals(Arrays.asList(book), jpaBookRepository.getBooksByUser(username));
    }

    @Test
    public void testGetBooksByUserShouldReturnNull() {
        // Given
        String username = "admin";
        User user = Mockito.mock(User.class);
        Book book = Mockito.mock(Book.class);

        // When
        when(ticketUserRepository.findByUsername(username)).thenReturn(Optional.of(user));
        when(bookDao.findBooksByUser(user)).thenReturn(Optional.empty());

        // Then
        Assertions.assertEquals(null, jpaBookRepository.getBooksByUser(username));
    }

    @Test
    public void testBook()
        throws InvalidSeatException, SeatAlreadyTakenException, RoomNotFoundException, MovieNotFoundException {
        //Given
        String movie = "movie";
        String room = "room";
        String date = "2021-01-01 01:01";
        String seats = "0,0";
        String username = "admin";
        Movie movieEntity = new Movie();
        movieEntity.setTitle(movie);
        Room roomEntity = new Room();
        roomEntity.setName(room);
        User user = new User(username, "admin", "ADMIN");
        Authentication res = new UsernamePasswordAuthenticationToken(username, "admin");

        // When
        when(movieDao.findByTitle(movie)).thenReturn(Optional.of(movieEntity));
        when(roomDao.findByName(room)).thenReturn(Optional.of(roomEntity));
        when(bookDao.findBooksByRoom(roomEntity)).thenReturn(Optional.empty());
        when(ticketUserRepository.findByUsername(any())).thenReturn(Optional.of(user));
        SecurityContextHolder.getContext().setAuthentication(res);

        jpaBookRepository.bookSeat(movie, room, date, seats);

        // Then
        verify(bookDao).save(any());
        verifyNoMoreInteractions(movieDao, roomDao, bookDao, ticketUserRepository);
    }
}
