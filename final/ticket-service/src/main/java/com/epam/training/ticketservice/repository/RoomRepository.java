package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository {
    void addRoom(Room room);

    List<Room> getRooms();

    void updateRoom(String name, int rows, int columns) throws RoomNotFoundException;

    void deleteRoom(String name) throws RoomNotFoundException;
}
