package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.exception.InvalidSeatException;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.SeatAlreadyTakenException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    Book bookSeat(String movie, String room, String startdate, String seats)
        throws MovieNotFoundException, RoomNotFoundException, InvalidSeatException, SeatAlreadyTakenException;

    List<Book> getBooksByUser(String username);
}
