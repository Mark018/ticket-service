package com.epam.training.ticketservice.cli.handler;

import com.epam.training.ticketservice.dataaccess.model.Screening;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningOverlapException;
import com.epam.training.ticketservice.service.ScreeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

import java.util.List;

@ShellComponent
public class ScreeningCommandHandler {
    @Autowired
    private ScreeningService screeningService;

    @ShellMethod(value = "Create new screening", key = "create screening")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public String createScreening(String movie, String room, String startdate) {
        try {
            screeningService.addScreening(movie, room, startdate);
        } catch (MovieNotFoundException e) {
            return "The specified movie not exists";
        } catch (RoomNotFoundException e) {
            return "The specified room not exists";
        } catch (ScreeningOverlapException e) {
            return e.getMessage();
        }
        return null;
    }

    @ShellMethod(value = "Delete a existing screening", key = "delete screening")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public String deleteScreening(String movie, String room, String startdate) {
        try {
            screeningService.deleteScreening(movie, room, startdate);
        } catch (RoomNotFoundException e) {
            return "The specified room not exists";
        } catch (ScreeningNotFoundException e) {
            return "The specified screening not exists";
        } catch (MovieNotFoundException e) {
            return "The specified movie not exists";
        }
        return null;
    }

    @ShellMethod(value = "List all screenings", key = "list screenings")
    public String listScreenings() {
        List<Screening> screeningList = screeningService.getScreenings();
        if (screeningList.size() == 0) {
            return "There are no screenings";
        } else {
            screeningList.forEach(System.out::println);
        }
        return null;
    }
}
