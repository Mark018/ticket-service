package com.epam.training.ticketservice.cli;

import com.epam.training.ticketservice.cli.handler.BookCommandHandler;
import com.epam.training.ticketservice.dataaccess.converter.Seat;
import com.epam.training.ticketservice.dataaccess.converter.SeatList;
import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.exception.InvalidSeatException;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.SeatAlreadyTakenException;
import com.epam.training.ticketservice.service.BookService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TestBookCommandHandler {
    @InjectMocks
    private BookCommandHandler bookCommandHandler;

    @Mock
    private BookService bookService;

    @Test
    public void testBookShouldBookTheSelectedSeat()
        throws InvalidSeatException, MovieNotFoundException, RoomNotFoundException, SeatAlreadyTakenException {
        // Given
        String movie = "movie";
        String room = "room";
        String startdate = "2021-01-01 01:01";
        String seats = "0,0";
        Book book = new Book();
        book.setSeats(SeatList.to(seats));

        // When
        when(bookService.bookSeat(movie, room, startdate, seats)).thenReturn(book);
        bookCommandHandler.book(movie, room, startdate, seats);

        // Then
        verify(bookService).bookSeat(movie, room, startdate, seats);
    }

    @Test
    public void testBookShouldBookTheSelectedSeatWithPrice()
        throws InvalidSeatException, MovieNotFoundException, RoomNotFoundException, SeatAlreadyTakenException {
        // Given
        String movie = "movie";
        String room = "room";
        String startdate = "2021-01-01 01:01";
        String seats = "0,0 1,2";
        Book book = new Book();
        book.setSeats(SeatList.to(seats));
        book.setCost(3000);

        // When
        when(bookService.bookSeat(movie, room, startdate, seats)).thenReturn(book);

        // Then
        Assertions.assertEquals("Seats booked: (0,0), (1,2); the price for this booking is 3000 HUF",
            bookCommandHandler.book(movie, room, startdate, seats));
    }

    @Test
    public void testBookShouldThrowInvalidSeatException()
        throws InvalidSeatException, MovieNotFoundException, RoomNotFoundException, SeatAlreadyTakenException {
        // Given
        String movie = "movie";
        String room = "room";
        String startdate = "2021-01-01 01:01";
        String seats = "0,0 1,2";

        // Then
        doThrow(new InvalidSeatException("seat")).when(bookService).bookSeat(movie, room, startdate, seats);
        bookCommandHandler.book(movie, room, startdate, seats);
    }

    @Test
    public void testBookShouldThrowMovieNotFoundException()
        throws InvalidSeatException, MovieNotFoundException, RoomNotFoundException, SeatAlreadyTakenException {
        // Given
        String movie = "movie";
        String room = "room";
        String startdate = "2021-01-01 01:01";
        String seats = "0,0 1,2";

        // Then
        doThrow(MovieNotFoundException.class).when(bookService).bookSeat(movie, room, startdate, seats);
        bookCommandHandler.book(movie, room, startdate, seats);
    }

    @Test
    public void testBookShouldThrowRoomNotFoundException()
        throws InvalidSeatException, MovieNotFoundException, RoomNotFoundException, SeatAlreadyTakenException {
        // Given
        String movie = "movie";
        String room = "room";
        String startdate = "2021-01-01 01:01";
        String seats = "0,0 1,2";

        // Then
        doThrow(RoomNotFoundException.class).when(bookService).bookSeat(movie, room, startdate, seats);
        bookCommandHandler.book(movie, room, startdate, seats);
    }

    @Test
    public void testBookShouldThrowSeatAlreadyTakenException()
        throws InvalidSeatException, MovieNotFoundException, RoomNotFoundException, SeatAlreadyTakenException {
        // Given
        String movie = "movie";
        String room = "room";
        String startdate = "2021-01-01 01:01";
        String seats = "0,0 1,2";

        // Then
        doThrow(SeatAlreadyTakenException.class).when(bookService).bookSeat(movie, room, startdate, seats);
        bookCommandHandler.book(movie, room, startdate, seats);
    }
}
