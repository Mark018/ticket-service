package com.epam.training.ticketservice.exception;

public class SeatAlreadyTakenException extends Exception {
    public SeatAlreadyTakenException(String message) {
        super(message);
    }
}
