package com.epam.training.ticketservice.repository.impl;

import com.epam.training.ticketservice.dataaccess.dao.MovieDao;
import com.epam.training.ticketservice.dataaccess.dao.RoomDao;
import com.epam.training.ticketservice.dataaccess.dao.ScreeningDao;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.dataaccess.model.Screening;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningOverlapException;
import com.epam.training.ticketservice.repository.ScreeningRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Repository
public class JpaScreeningRepository implements ScreeningRepository {
    @Autowired
    private ScreeningDao screeningDao;

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private RoomDao roomDao;

    @Override
    public void addScreening(String movie, String room, String startdate)
        throws MovieNotFoundException, RoomNotFoundException, ScreeningOverlapException {
        Optional<Movie> findedMovie = movieDao.findByTitle(movie);
        if (!findedMovie.isPresent()) {
            throw new MovieNotFoundException();
        }
        Optional<Room> findedRoom = roomDao.findByName(room);
        if (!findedRoom.isPresent()) {
            throw new RoomNotFoundException();
        }

        LocalDateTime localTime = LocalDateTime.parse(startdate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        ZonedDateTime time = localTime.atZone(ZoneId.systemDefault());

        Optional<List<Screening>> byRoom = screeningDao.findByRoom(findedRoom.get());
        if (byRoom.isPresent()) {
            Optional<Screening> findedScreening =
                StreamSupport.stream(byRoom.get().spliterator(), false).filter(
                    entity -> {
                        return ((entity.getStartTime().isAfter(time) || entity.getStartTime().isEqual(time))
                            && time.plusMinutes(findedMovie.get().getDuration()).isAfter(entity.getStartTime()))
                            || (time.isAfter(entity.getStartTime())
                            && time.isBefore(entity.getStartTime().plusMinutes(entity.getMovie().getDuration())));
                    }
                ).findAny();
            if (findedScreening.isPresent()) {
                throw new ScreeningOverlapException("There is an overlapping screening");
            }

            Optional<Screening> findedScreeningInBreakPeriod =
                StreamSupport.stream(byRoom.get().spliterator(), false).filter(
                    entity -> {
                        return ((entity.getStartTime().isAfter(time) || entity.getStartTime().isEqual(time))
                            && time.plusMinutes(findedMovie.get().getDuration() + 10).isAfter(entity.getStartTime()))
                            || (time.isAfter(entity.getStartTime())
                            && time.isBefore(entity.getStartTime().plusMinutes(entity.getMovie().getDuration() + 10)));
                    }
                ).findAny();
            if (findedScreeningInBreakPeriod.isPresent()) {
                throw new ScreeningOverlapException(
                    "This would start in the break period after another screening in this room");
            }
        }

        screeningDao.save(new Screening(findedMovie.get(), findedRoom.get(), time));
    }

    @Override
    public void deleteScreening(String movie, String room, String startdate)
        throws MovieNotFoundException, RoomNotFoundException, ScreeningNotFoundException {
        Optional<Movie> findedMovie = movieDao.findByTitle(movie);
        if (!findedMovie.isPresent()) {
            throw new MovieNotFoundException();
        }
        Optional<Room> findedRoom = roomDao.findByName(room);
        if (!findedRoom.isPresent()) {
            throw new RoomNotFoundException();
        }

        LocalDateTime localTime = LocalDateTime.parse(startdate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        ZonedDateTime time = localTime.atZone(ZoneId.systemDefault());

        Optional<Screening> findedScreening = StreamSupport.stream(screeningDao.findAll().spliterator(), false).filter(
            entity -> {
                return entity.getMovie().equals(findedMovie.get())
                    && entity.getRoom().equals(findedRoom.get())
                    && entity.getStartTime().equals(time);
            }
        ).findAny();
        if (!findedScreening.isPresent()) {
            throw new ScreeningNotFoundException();
        }

        screeningDao.delete(findedScreening.get());
    }

    @Override
    public List<Screening> getScreenings() {
        return screeningDao.findAll();
    }
}
