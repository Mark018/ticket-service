package com.epam.training.ticketservice.cli.handler;

import com.epam.training.ticketservice.cli.UserCommandAvailability;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

import java.util.List;

@ShellComponent
public class MovieCommandHandler extends UserCommandAvailability {
    @Autowired
    private MovieService movieService;

    @ShellMethod(value = "Create new movie", key = "create movie")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public void createMovie(String title, String genre, int duration) {
        movieService.addMovie(title, genre, duration);
    }

    @ShellMethod(value = "Update a existing movie", key = "update movie")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public String updateMovie(String title, String genre, int duration) {
        try {
            movieService.updateMovie(title, genre, duration);
        } catch (MovieNotFoundException e) {
            return "The specified movie not exists";
        }
        return null;
    }

    @ShellMethod(value = "Delete a existing movie", key = "delete movie")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public String deleteMovie(String title) {
        try {
            movieService.deleteMovie(title);
        } catch (MovieNotFoundException e) {
            return "The specified movie not exists";
        }
        return null;
    }

    @ShellMethod(value = "List all movies", key = "list movies")
    public String listMovies() {
        List<Movie> movieList = movieService.getMovies();
        if (movieList.size() == 0) {
            return "There are no movies at the moment";
        } else {
            movieList.forEach(System.out::println);
        }
        return null;
    }
}
