package com.epam.training.ticketservice.cli.handler;

import com.epam.training.ticketservice.exception.InsufficientPermissionException;
import com.epam.training.ticketservice.service.TicketUserDetailsService;
import com.epam.training.ticketservice.cli.UserCommandAvailability;
import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

import java.util.List;
import java.util.stream.Collectors;

@ShellComponent
public class AuthenticationCommandHandler extends UserCommandAvailability {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TicketUserDetailsService ticketUserDetailsService;

    @Autowired
    private BookRepository bookRepository;

    @ShellMethod(value = "Sign in a specified account with username and password", key = "sign in privileged")
    @ShellMethodAvailability("isUserSignedOut")
    public String singIn(String username, String password) {
        Authentication req = new UsernamePasswordAuthenticationToken(username, password);

        try {
            Authentication result = authenticationManager.authenticate(req);
            if (result.getAuthorities().stream().noneMatch(a -> a.getAuthority().equals("ADMIN"))) {
                throw new InsufficientPermissionException();
            }
            SecurityContextHolder.getContext().setAuthentication(result);
            return null;
        } catch (AuthenticationException e) {
            return "Login failed due to incorrect credentials";
        } catch (InsufficientPermissionException e) {
            return "Insufficient permission";
        }
    }

    @ShellMethod(value = "Sign out from a logged in account", key = "sign out")
    @ShellMethodAvailability("isUserSignedIn")
    public String signOut() {
        SecurityContextHolder.clearContext();
        return null;
    }

    @ShellMethod(value = "Show the actual user information", key = "describe account")
    public String describeAccount() {
        if (isUserSignedIn().isAvailable()) {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            if (isUserHasAdminPrivileges().isAvailable()) {
                return "Signed in with privileged account '"
                    + username + "'";
            } else {
                String bookingText;

                List<Book> books = bookRepository.getBooksByUser(username);

                if (books == null || books.isEmpty()) {
                    bookingText = "You have not booked any tickets yet";
                } else {
                    bookingText = "Your previous bookings are\n"
                        + books.stream().map(e -> e.toString()).collect(Collectors.joining("\n"));
                }

                return "Signed in with account '"
                    + SecurityContextHolder.getContext().getAuthentication().getName()
                    + "'\n" + bookingText;
            }

        } else {
            return "You are not signed in";
        }
    }

    @ShellMethod(value = "Register new user account", key = "sign up")
    public String signUp(String username, String password) {
        if (ticketUserDetailsService.isUserExists(username)) {
            return "User is already exists!";
        }
        ticketUserDetailsService.addUser(username, password);
        return null;
    }

    @ShellMethod(value = "Sign in a specified account with username and password (user account)", key = "sign in")
    @ShellMethodAvailability("isUserSignedOut")
    public String singInToPersonal(String username, String password) {
        Authentication req = new UsernamePasswordAuthenticationToken(username, password);

        try {
            Authentication result = authenticationManager.authenticate(req);
            if (result.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"))) {
                return "Please use the 'sign in privileged' command.";
            }
            SecurityContextHolder.getContext().setAuthentication(result);
            return null;
        } catch (AuthenticationException e) {
            return "Login failed due to incorrect credentials";
        }
    }
}
