package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.exception.InvalidSeatException;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.SeatAlreadyTakenException;
import com.epam.training.ticketservice.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestBookService {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookService bookService;

    @Test
    public void testBookSeatShouldInvokeTheRepositoryBookSeat()
        throws InvalidSeatException, MovieNotFoundException, RoomNotFoundException, SeatAlreadyTakenException {
        // Given
        String movie = "movie";
        String room = "room";
        String date = "date";
        String seat = "seat";

        // When
        bookService.bookSeat(movie, room, date, seat);

        // Then
        verify(bookRepository).bookSeat(movie, room, date, seat);
        Mockito.verifyNoMoreInteractions(bookRepository);
    }
}
