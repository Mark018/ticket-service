package com.epam.training.ticketservice.repository.impl;

import com.epam.training.ticketservice.dataaccess.dao.MovieDao;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public class JpaMovieRepository implements MovieRepository {
    @Autowired
    private MovieDao movieDao;

    @Override
    @Transactional
    public void addMovie(Movie movie) {
        movieDao.save(movie);
    }

    @Override
    @Transactional
    public List<Movie> getMovies() {
        return movieDao.findAll();
    }

    @Override
    public void updateMovie(String title, String genre, int duration) throws MovieNotFoundException {
        Optional<Movie> opt = movieDao.findByTitle(title);
        if (!opt.isPresent()) {
            throw new MovieNotFoundException();
        }

        Movie element = opt.get();
        element.setGenre(genre);
        element.setDuration(duration);
        movieDao.save(element);
    }

    @Override
    public void deleteMovie(String title) throws MovieNotFoundException {
        Optional<Movie> opt = movieDao.findByTitle(title);
        if (!opt.isPresent()) {
            throw new MovieNotFoundException();
        }
        movieDao.delete(opt.get());
    }

}
