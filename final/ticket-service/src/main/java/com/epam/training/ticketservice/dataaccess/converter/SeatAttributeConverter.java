package com.epam.training.ticketservice.dataaccess.converter;

import javax.persistence.AttributeConverter;

public class SeatAttributeConverter implements AttributeConverter<SeatList, String> {
    @Override
    public String convertToDatabaseColumn(SeatList attribute) {
        if (attribute != null) {
            return attribute.toString();
        }

        return null;
    }

    @Override
    public SeatList convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return SeatList.to(dbData);
        }

        return null;
    }
}
