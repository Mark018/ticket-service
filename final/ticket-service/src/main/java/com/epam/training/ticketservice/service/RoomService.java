package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {
    @Autowired
    private RoomRepository roomRepository;

    public void addRoom(String name, int rows, int columns) {
        roomRepository.addRoom(new Room(name, rows, columns));
    }

    public void updateRoom(String name, int rows, int columns) throws RoomNotFoundException {
        roomRepository.updateRoom(name, rows, columns);
    }

    public void deleteRoom(String title) throws RoomNotFoundException {
        roomRepository.deleteRoom(title);
    }

    public List<Room> getRooms() {
        return roomRepository.getRooms();
    }
}
