package com.epam.training.ticketservice.converter;

import com.epam.training.ticketservice.dataaccess.converter.Seat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSeat {
    @Test
    public void testSetAndGetRow() {
        // Given
        Seat seat = new Seat(0, 0);

        // When
        seat.setRow(1);

        // Then
        Assertions.assertEquals(1, seat.getRow());
    }

    @Test
    public void testSetAndGetColumn() {
        // Given
        Seat seat = new Seat(0, 0);

        // When
        seat.setColumn(1);

        // Then
        Assertions.assertEquals(1, seat.getColumn());
    }
}
