package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.dataaccess.dao.MovieDao;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.repository.impl.JpaMovieRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestMovieRepository {

    @Mock
    private MovieDao movieDao;

    @InjectMocks
    private JpaMovieRepository movieRepository;

    @Test
    public void testAddMovieShouldAddTheNewMovie() {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);

        // When
        movieRepository.addMovie(newMovie);

        // Then
        verify(movieDao).save(newMovie);
        Mockito.verifyNoMoreInteractions(newMovie);
    }

    @Test
    public void testUpdateMovieShouldThrowAMovieNotFoundExceptionWhenTheMovieNotFound() {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);

        // When / Then
        Assertions.assertThrows(MovieNotFoundException.class,
            () -> movieRepository.updateMovie(newMovie.getTitle(), "drama", 120));
    }

    @Test
    public void testUpdateMovieShouldUpdateTheMovieGenreAndDuration() throws MovieNotFoundException {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);

        // When
        Mockito.when(movieDao.findByTitle(newMovie.getTitle())).thenReturn(Optional.of(newMovie));
        movieRepository.updateMovie(newMovie.getTitle(), "abc", 123);

        // Then
        verify(movieDao).save(newMovie);
        Mockito.verifyNoMoreInteractions(movieDao);
    }

    @Test
    public void testGetMoviesShouldReturnsEmptyArray() {
        // Given
        List<Movie> empty = new ArrayList<Movie>();

        // When

        // Then
        Assertions.assertEquals(movieRepository.getMovies(), empty);
    }

    @Test
    public void testGetMoviesShouldReturnsTheExceptedArray() {
        // Given
        List<Movie> excepted = new ArrayList<Movie>();
        excepted.add(Mockito.mock(Movie.class));

        // When
        Mockito.when(movieDao.findAll()).thenReturn(excepted);

        // Then
        Assertions.assertEquals(movieRepository.getMovies(), excepted);
    }

    @Test
    public void testDeleteMovieShouldThrowAMovieNotFoundExceptionWhenTheMovieNotFound() {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);

        // When / Then
        Assertions.assertThrows(MovieNotFoundException.class,
            () -> movieRepository.deleteMovie(newMovie.getTitle()));
    }

    @Test
    public void testDeleteMovieShouldDeleteTheSelectedMovie() throws MovieNotFoundException {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);

        // When
        Mockito.when(movieDao.findByTitle(newMovie.getTitle())).thenReturn(Optional.of(newMovie));
        movieRepository.deleteMovie(newMovie.getTitle());

        // Then
        verify(movieDao).delete(newMovie);
        Mockito.verifyNoMoreInteractions(movieDao);
    }
}
