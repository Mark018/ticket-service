package com.epam.training.ticketservice.exception;

public class ScreeningOverlapException extends Exception {
    public ScreeningOverlapException(String message) {
        super(message);
    }
}
