package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.TicketUserDetails;
import com.epam.training.ticketservice.dataaccess.model.User;
import com.epam.training.ticketservice.dataaccess.dao.TicketUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

public class TicketUserDetailsService implements UserDetailsService {
    private PasswordEncoder passwordEncoder;

    private final TicketUserDao ticketUserRepository;

    public TicketUserDetailsService(TicketUserDao tickerUserRepository) {
        this.ticketUserRepository = tickerUserRepository;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = ticketUserRepository.findByUsername(username);
        ((Optional<?>) user).orElseThrow(() -> new UsernameNotFoundException("User not found " + username));
        return user.map(TicketUserDetails::new).get();
    }

    public boolean isUserExists(String username) {
        return ticketUserRepository.findByUsername(username).isPresent();
    }

    public void addUser(String username, String password) {
        ticketUserRepository.save(new User(username, passwordEncoder.encode(password), "USER"));
    }
}
