package com.epam.training.ticketservice.dataaccess.configuration;

import com.epam.training.ticketservice.dataaccess.model.User;
import com.epam.training.ticketservice.dataaccess.dao.TicketUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Profile("! prod")
public class DefaultAdminDatabaseInitializer {
    @Autowired
    private PasswordEncoder passwordEncoder;

    private TicketUserDao ticketUserRepository;

    public DefaultAdminDatabaseInitializer(TicketUserDao ticketUserRepository) {
        this.ticketUserRepository = ticketUserRepository;
    }

    @PostConstruct
    public void initDefaultAdminUser() {
        this.ticketUserRepository.save(new User("admin", passwordEncoder.encode("admin"), "ADMIN"));
        this.ticketUserRepository.save(new User("user", passwordEncoder.encode("user"), "USER"));
        System.out.println("Added default admin account to database!");
    }
}
