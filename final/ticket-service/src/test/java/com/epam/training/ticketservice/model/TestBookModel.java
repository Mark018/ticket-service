package com.epam.training.ticketservice.model;

import com.epam.training.ticketservice.dataaccess.model.User;
import com.epam.training.ticketservice.dataaccess.converter.SeatList;
import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.dataaccess.model.Room;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TestBookModel {

    @Test
    public void testSetAndGetId() {
        // Given
        Book book = new Book();

        // When
        book.setId(1);

        // Then
        Assertions.assertEquals(1, book.getId());
    }

    @Test
    public void testSetAndGetMovie() {
        // Given
        Book book = new Book();
        Movie movie = Mockito.mock(Movie.class);

        // When
        book.setMovie(movie);

        // Then
        Assertions.assertEquals(movie, book.getMovie());
    }

    @Test
    public void testSetAndGetRoom() {
        // Given
        Book book = new Book();
        Room room = Mockito.mock(Room.class);

        // When
        book.setRoom(room);

        // Then
        Assertions.assertEquals(room, book.getRoom());
    }

    @Test
    public void testSetAndGetUser() {
        // Given
        Book book = new Book();
        User user = Mockito.mock(User.class);

        // When
        book.setUser(user);

        // Then
        Assertions.assertEquals(user, book.getUser());
    }

    @Test
    public void testSetAndGetStartTime() {
        // Given
        Book book = new Book();
        ZonedDateTime time = ZonedDateTime.now();

        // When
        book.setStarttime(time);

        // Then
        Assertions.assertEquals(time, book.getStarttime());
    }

    @Test
    public void testSetAndGetSeats() {
        // Given
        Book book = new Book();
        SeatList sl = SeatList.to("0,0");

        // When
        book.setSeats(sl);

        // Then
        Assertions.assertEquals(sl, book.getSeats());
    }

    @Test
    public void testSetAndGetCost() {
        // Given
        Book book = new Book();

        // When
        book.setCost(1);

        // Then
        Assertions.assertEquals(1, book.getCost());
    }

    @Test
    public void testBookFullArgumentConstructor() {
        // Given
        Room room = Mockito.mock(Room.class);
        Movie movie = Mockito.mock(Movie.class);
        User user = Mockito.mock(User.class);

        Book book = new Book(movie, room, ZonedDateTime.now(), SeatList.to("0,0"), 1, user);

        // Then / When
        Assertions.assertEquals(movie, book.getMovie());
        Assertions.assertEquals(room, book.getRoom());
        Assertions.assertEquals(user, book.getUser());
    }

    @Test
    public void testBookHashCodeIsNotEqual() {
        // Given
        Room room = Mockito.mock(Room.class);
        Movie movie = Mockito.mock(Movie.class);
        User user = Mockito.mock(User.class);

        Book book = new Book(movie, room, ZonedDateTime.now(), SeatList.to("0,0"), 1, user);
        Book book2 = new Book(movie, room, ZonedDateTime.now(), SeatList.to("0,0"), 2, user);

        // Then / When
        Assertions.assertFalse(book.equals(book2));
    }

    @Test
    public void testBookHashCodeIsSame() {
        // Given
        Room room = Mockito.mock(Room.class);
        Movie movie = Mockito.mock(Movie.class);
        User user = Mockito.mock(User.class);

        Book book = new Book(movie, room, ZonedDateTime.now(), SeatList.to("0,0"), 1, user);

        // Then / When
        Assertions.assertTrue(book.equals(book));
    }

    @Test
    public void testBookHashCodeIsNull() {
        // Given
        Room room = Mockito.mock(Room.class);
        Movie movie = Mockito.mock(Movie.class);
        User user = Mockito.mock(User.class);

        Book book = new Book(movie, room, ZonedDateTime.now(), SeatList.to("0,0"), 1, user);

        // Then / When
        Assertions.assertFalse(book.equals(null));
    }

    @Test
    public void testBookToString() {
        // Given
        Room room = Mockito.mock(Room.class);
        Movie movie = Mockito.mock(Movie.class);
        User user = Mockito.mock(User.class);

        ZonedDateTime time = ZonedDateTime.now();

        Book book = new Book(movie, room, ZonedDateTime.now(), SeatList.to("0,0"), 1, user);

        // Then / When
        Assertions.assertEquals(String.format("Seats (0,0) on null in room null starting at %s for 1 HUF", time.format(
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))), book.toString());
    }
}
