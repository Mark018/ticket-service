package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.exception.InvalidSeatException;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.SeatAlreadyTakenException;
import com.epam.training.ticketservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public Book bookSeat(String movie, String room, String startdate, String seats)
        throws InvalidSeatException, SeatAlreadyTakenException, RoomNotFoundException, MovieNotFoundException {
        return bookRepository.bookSeat(movie, room, startdate, seats);
    }
}
