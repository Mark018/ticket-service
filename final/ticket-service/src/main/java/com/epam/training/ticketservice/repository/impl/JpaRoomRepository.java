package com.epam.training.ticketservice.repository.impl;

import com.epam.training.ticketservice.dataaccess.dao.RoomDao;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class JpaRoomRepository implements RoomRepository {
    @Autowired
    private RoomDao roomDao;

    @Override
    public void addRoom(Room room) {
        roomDao.save(room);
    }

    @Override
    public List<Room> getRooms() {
        return roomDao.findAll();
    }

    @Override
    public void updateRoom(String name, int rows, int columns) throws RoomNotFoundException {
        Optional<Room> opt = roomDao.findByName(name);
        if (!opt.isPresent()) {
            throw new RoomNotFoundException();
        }

        Room element = opt.get();
        element.setRows(rows);
        element.setColumns(columns);
        roomDao.save(element);
    }

    @Override
    public void deleteRoom(String name) throws RoomNotFoundException {
        Optional<Room> opt = roomDao.findByName(name);
        if (!opt.isPresent()) {
            throw new RoomNotFoundException();
        }
        roomDao.delete(opt.get());
    }
}
