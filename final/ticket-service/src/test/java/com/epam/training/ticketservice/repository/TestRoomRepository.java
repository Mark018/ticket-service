package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.dataaccess.dao.RoomDao;
import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.repository.impl.JpaRoomRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestRoomRepository {

    @Mock
    private RoomDao roomDao;

    @InjectMocks
    private JpaRoomRepository roomRepository;

    @Test
    public void testAddRoomShouldAddTheNewRoom() {
        // Given
        Room newRoom = Mockito.mock(Room.class);

        // When
        roomRepository.addRoom(newRoom);

        // Then
        verify(roomDao).save(newRoom);
        Mockito.verifyNoMoreInteractions(newRoom);
    }

    @Test
    public void testUpdateRoomShouldThrowARoomNotFoundExceptionWhenTheRoomNotFound() {
        // Given
        Room newRoom = Mockito.mock(Room.class);

        // When / Then
        Assertions.assertThrows(RoomNotFoundException.class,
            () -> roomRepository.updateRoom(newRoom.getName(), 1, 1));
    }

    @Test
    public void testUpdateRoomShouldUpdateTheRoomRowsAndColumns() throws RoomNotFoundException {
        // Given
        Room newRoom = Mockito.mock(Room.class);

        // When
        Mockito.when(roomDao.findByName(newRoom.getName())).thenReturn(Optional.of(newRoom));
        roomRepository.updateRoom(newRoom.getName(), 1, 1);

        // Then
        verify(roomDao).save(newRoom);
        Mockito.verifyNoMoreInteractions(roomDao);
    }

    @Test
    public void testGetRoomsShouldReturnsEmptyArray() {
        // Given
        List<Room> empty = new ArrayList<Room>();

        // When

        // Then
        Assertions.assertEquals(roomRepository.getRooms(), empty);
    }

    @Test
    public void testGetRoomsShouldReturnsTheExceptedArray() {
        // Given
        List<Room> excepted = new ArrayList<Room>();
        excepted.add(Mockito.mock(Room.class));

        // When
        Mockito.when(roomDao.findAll()).thenReturn(excepted);

        // Then
        Assertions.assertEquals(roomRepository.getRooms(), excepted);
    }

    @Test
    public void testDeleteRoomShouldThrowARoomNotFoundExceptionWhenTheRoomNotFound() {
        // Given
        Room newRoom = Mockito.mock(Room.class);

        // When / Then
        Assertions.assertThrows(RoomNotFoundException.class,
            () -> roomRepository.deleteRoom(newRoom.getName()));
    }

    @Test
    public void testDeleteRoomShouldDeleteTheSelectedRoom() throws RoomNotFoundException {
        // Given
        Room newRoom = Mockito.mock(Room.class);

        // When
        Mockito.when(roomDao.findByName(newRoom.getName())).thenReturn(Optional.of(newRoom));
        roomRepository.deleteRoom(newRoom.getName());

        // Then
        verify(roomDao).delete(newRoom);
        Mockito.verifyNoMoreInteractions(roomDao);
    }
}
