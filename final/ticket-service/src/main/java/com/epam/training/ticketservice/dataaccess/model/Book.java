package com.epam.training.ticketservice.dataaccess.model;

import com.epam.training.ticketservice.dataaccess.converter.SeatAttributeConverter;
import com.epam.training.ticketservice.dataaccess.converter.SeatList;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;

    private ZonedDateTime starttime;

    @Convert(converter = SeatAttributeConverter.class)
    private SeatList seats;

    private int cost;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Book() {
    }

    public Book(Movie movie, Room room, ZonedDateTime startTime, SeatList seats, int cost, User user) {
        this.movie = movie;
        this.room = room;
        this.starttime = startTime;
        this.seats = seats;
        this.cost = cost;
        this.user = user;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Movie getMovie() {
        return this.movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Room getRoom() {
        return this.room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public ZonedDateTime getStarttime() {
        return this.starttime;
    }

    public void setStarttime(ZonedDateTime datetime) {
        this.starttime = datetime;
    }

    public SeatList getSeats() {
        return this.seats;
    }

    public void setSeats(SeatList seats) {
        this.seats = seats;
    }

    public int getCost() {
        return this.cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return String.format("Seats %s on %s in room %s starting at %s for %d HUF",
            this.seats.toString(", ", "(", ")"),
            this.movie.getTitle(),
            this.room.getName(),
            this.starttime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")),
            this.cost);
    }
}
