package com.epam.training.ticketservice.model;

import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.dataaccess.model.Screening;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TestScreeningModel {

    @Test
    public void testSetAndGetId() {
        // Given
        Screening screening = new Screening();

        // When
        screening.setId(1);

        // Then
        Assertions.assertEquals(1, screening.getId());
    }

    @Test
    public void testSetAndGetMovie() {
        // Given
        Screening screening = new Screening();
        Movie movie = Mockito.mock(Movie.class);

        // When
        screening.setMovie(movie);

        // Then
        Assertions.assertEquals(movie, screening.getMovie());
    }

    @Test
    public void testSetAndGetRoom() {
        // Given
        Screening screening = new Screening();
        Room room = Mockito.mock(Room.class);

        // When
        screening.setRoom(room);

        // Then
        Assertions.assertEquals(room, screening.getRoom());
    }

    @Test
    public void testSetAndGetStartTime() {
        // Given
        Screening screening = new Screening();
        ZonedDateTime time = ZonedDateTime.now();

        // When
        screening.setStartTime(time);

        // Then
        Assertions.assertEquals(time, screening.getStartTime());
    }

    @Test
    public void testRoomFullArgumentConstructor() {
        // Given
        Movie movie = Mockito.mock(Movie.class);
        Room room = Mockito.mock(Room.class);
        ZonedDateTime time = ZonedDateTime.now();
        Screening screening = new Screening(movie, room, time);

        // Then / When
        Assertions.assertEquals(movie, screening.getMovie());
        Assertions.assertEquals(room, screening.getRoom());
        Assertions.assertEquals(time, screening.getStartTime());
    }

    @Test
    public void testRoomToString() {
        // Given
        Movie movie = Mockito.mock(Movie.class);
        Room room = Mockito.mock(Room.class);
        ZonedDateTime time = ZonedDateTime.now();
        Screening screening = new Screening(movie, room, time);

        // Then / When
        Assertions.assertEquals(String.format("null (null, 0 minutes), screened in room null, at %s", time.format(
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))), screening.toString());
    }
}
