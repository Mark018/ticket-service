package com.epam.training.ticketservice.model;

import com.epam.training.ticketservice.dataaccess.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestUserModel {

    @Test
    public void testSetAndGetId() {
        // Given
        User user = new User();

        // When
        user.setId(1);

        // Then
        Assertions.assertEquals(1, user.getId());
    }

    @Test
    public void testSetAndGetUsername() {
        // Given
        User user = new User();

        // When
        user.setUsername("test");

        // Then
        Assertions.assertEquals("test", user.getUsername());
    }

    @Test
    public void testSetAndGetPassword() {
        // Given
        User user = new User();

        // When
        user.setPassword("test");

        // Then
        Assertions.assertEquals("test", user.getPassword());
    }

    @Test
    public void testSetAndGetRoles() {
        // Given
        User user = new User();

        // When
        user.setRoles("test");

        // Then
        Assertions.assertEquals("test", user.getRoles());
    }

    @Test
    public void testBookFullArgumentConstructor() {
        // Given
        User user = new User("uname", "pw", "role");

        // Then / When
        Assertions.assertEquals("uname", user.getUsername());
        Assertions.assertEquals("pw", user.getPassword());
        Assertions.assertEquals("role", user.getRoles());
    }

    @Test
    public void testBookToString() {
        // Given
        User user = new User("uname", "pw", "role");

        // Then / When
        Assertions.assertEquals("User{id=0, username='uname', password='pw', roles='role'}", user.toString());
    }
}
