package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.dataaccess.model.Movie;

import java.util.List;

public interface MovieRepository {
    void addMovie(Movie movie);

    List<Movie> getMovies();

    void updateMovie(String title, String genre, int duration) throws MovieNotFoundException;

    void deleteMovie(String title) throws MovieNotFoundException;
}
