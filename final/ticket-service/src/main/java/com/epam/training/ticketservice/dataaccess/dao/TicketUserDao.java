package com.epam.training.ticketservice.dataaccess.dao;

import com.epam.training.ticketservice.dataaccess.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TicketUserDao extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
}
