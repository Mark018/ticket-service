package com.epam.training.ticketservice.dataaccess.converter;

import java.util.Objects;

public class Seat {
    private int row;
    private int column;

    public Seat(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getColumn() {
        return this.column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return this.row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public static Seat of(String str) {
        String[] args = str.split(",");
        try {
            return new Seat(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return row + "," + column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Seat seat = (Seat) o;
        return row == seat.row
            && column == seat.column;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }
}
