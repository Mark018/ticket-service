package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.Screening;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningOverlapException;
import com.epam.training.ticketservice.repository.ScreeningRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScreeningService {
    @Autowired
    private ScreeningRepository screeningRepository;

    public void addScreening(String movie, String room, String startdate)
        throws RoomNotFoundException, MovieNotFoundException, ScreeningOverlapException {
        screeningRepository.addScreening(movie, room, startdate);
    }

    public void deleteScreening(String movie, String room, String startdate)
        throws ScreeningNotFoundException, RoomNotFoundException, MovieNotFoundException {
        screeningRepository.deleteScreening(movie, room, startdate);
    }

    public List<Screening> getScreenings() {
        return screeningRepository.getScreenings();
    }
}
