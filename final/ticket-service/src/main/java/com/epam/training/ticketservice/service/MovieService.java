package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;

    public void addMovie(String title, String genre, int duration) {
        Movie newMovie = new Movie(title, genre, duration);
        movieRepository.addMovie(newMovie);
    }

    public void updateMovie(String title, String genre, int duration) throws MovieNotFoundException {
        movieRepository.updateMovie(title, genre, duration);
    }

    public void deleteMovie(String title) throws MovieNotFoundException {
        movieRepository.deleteMovie(title);
    }

    public List<Movie> getMovies() {
        return movieRepository.getMovies();
    }
}
