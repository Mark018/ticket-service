package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.repository.RoomRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestRoomService {

    @Mock
    private RoomRepository roomRepository;

    @InjectMocks
    private RoomService roomService;

    @Test
    public void testAddRoomShouldInvokeTheAddRoom() {
        // Given
        Room room = new Room("test", 1, 1);

        // When
        roomService.addRoom(room.getName(), room.getRows(), room.getColumns());

        // Then
        verify(roomRepository).addRoom(room);
        Mockito.verifyNoMoreInteractions(roomRepository);
    }

    @Test
    public void testUpdateRoomShouldInvokeTheUpdateRoom() throws RoomNotFoundException {
        // Given
        Room room = new Room("test", 1, 1);

        // When
        roomService.updateRoom(room.getName(), room.getRows(), room.getColumns());

        // Then
        verify(roomRepository).updateRoom(room.getName(), room.getRows(), room.getColumns());
        Mockito.verifyNoMoreInteractions(roomRepository);
    }

    @Test
    public void testDeleteRoomShouldInvokeTheDeleteRoom() throws RoomNotFoundException {
        // Given
        Room room = new Room("test", 1, 1);

        // When
        roomService.deleteRoom(room.getName());

        // Then
        verify(roomRepository).deleteRoom(room.getName());
        Mockito.verifyNoMoreInteractions(roomRepository);
    }

    @Test
    public void testGetRoomsShouldInvokeTheRepositoryGetRooms() {
        // When
        roomService.getRooms();

        // Then
        verify(roomRepository).getRooms();
        Mockito.verifyNoMoreInteractions(roomRepository);
    }
}
