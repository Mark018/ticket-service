package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.dataaccess.dao.MovieDao;
import com.epam.training.ticketservice.dataaccess.dao.RoomDao;
import com.epam.training.ticketservice.dataaccess.dao.ScreeningDao;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.dataaccess.model.Screening;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningOverlapException;
import com.epam.training.ticketservice.repository.impl.JpaRoomRepository;
import com.epam.training.ticketservice.repository.impl.JpaScreeningRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.text.html.Option;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestScreeningRepository {
    @Mock
    private ScreeningDao screeningDao;

    @Mock
    private MovieDao movieDao;

    @Mock
    private RoomDao roomDao;

    @InjectMocks
    private JpaScreeningRepository screeningRepository;

    @Test
    public void testAddScreeningShouldAddTheNewScreening()
        throws RoomNotFoundException, MovieNotFoundException, ScreeningOverlapException {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);
        Room newRoom = Mockito.mock(Room.class);
        String startdate = "2021-01-01 00:00";

        // When
        Mockito.when(movieDao.findByTitle(newMovie.getTitle())).thenReturn(Optional.of(newMovie));
        Mockito.when(roomDao.findByName(newRoom.getName())).thenReturn(Optional.of(newRoom));
        Mockito.when(screeningDao.findByRoom(Mockito.any())).thenReturn(Optional.empty());

        screeningRepository.addScreening(newMovie.getTitle(), newRoom.getName(), startdate);

        // Then
        verify(screeningDao, times(1)).save(Mockito.any());
    }

    @Test
    public void testAddScreeningThrowMovieNotFoundExceptionWhenTheMovieNotFound() {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);
        Room newRoom = Mockito.mock(Room.class);
        String startdate = "2021-01-01 00:00";

        // When / Then
        Assertions.assertThrows(MovieNotFoundException.class,
            () -> screeningRepository.addScreening(newMovie.getTitle(), newRoom.getName(), startdate));
    }

    @Test
    public void testAddScreeningThrowScreeningOverlapExceptionWhenTheRoomIsBusy() {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);
        Room newRoom = Mockito.mock(Room.class);
        String startdate = "2021-01-01 00:00";

        // When
        Mockito.when(movieDao.findByTitle(newMovie.getTitle())).thenReturn(Optional.of(newMovie));

        // Then
        Assertions.assertThrows(RoomNotFoundException.class,
            () -> screeningRepository.addScreening(newMovie.getTitle(), newRoom.getName(), startdate));
    }

    @Test
    public void testAddScreeningThrowRoomNotFoundExceptionWhenTheRoomNotFound() {
        // Given
        Movie newMovie = Mockito.mock(Movie.class);
        Room newRoom = Mockito.mock(Room.class);
        String startdate = "2021-01-01 00:00";

        LocalDateTime localTime = LocalDateTime.parse(startdate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        ZonedDateTime time = localTime.atZone(ZoneId.systemDefault());
        Screening newScreening = new Screening(newMovie, newRoom, time);

        // When
        Mockito.when(movieDao.findByTitle(newMovie.getTitle())).thenReturn(Optional.of(newMovie));
        Mockito.when(roomDao.findByName(newRoom.getName())).thenReturn(Optional.of(newRoom));
        Mockito.when(screeningDao.findByRoom(Mockito.any())).thenReturn(Optional.of(Arrays.asList(newScreening)));

        // Then
        Assertions.assertThrows(ScreeningOverlapException.class,
            () -> screeningRepository.addScreening(newMovie.getTitle(), newRoom.getName(), startdate));
    }


}
