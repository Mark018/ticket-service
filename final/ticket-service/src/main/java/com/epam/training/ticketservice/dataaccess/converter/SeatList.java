package com.epam.training.ticketservice.dataaccess.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class SeatList extends ArrayList<Seat> {
    @Override
    public String toString() {
        return String.join(" ", this.stream().map(e -> e.toString()).collect(Collectors.toList()));
    }

    public String toString(String delimiter, String prefix, String suffix) {
        return String
            .join(delimiter, this.stream().map(e -> prefix + e.toString() + suffix).collect(Collectors.toList()));
    }

    public static SeatList to(String str) {
        SeatList lst = new SeatList();
        if (!str.isEmpty()) {
            Arrays.stream(str.split(" ")).forEach(e -> lst.add(Seat.of(e)));
        }
        return lst;
    }
}
