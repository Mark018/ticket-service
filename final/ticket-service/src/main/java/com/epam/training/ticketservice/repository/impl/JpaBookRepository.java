package com.epam.training.ticketservice.repository.impl;

import com.epam.training.ticketservice.dataaccess.model.User;
import com.epam.training.ticketservice.dataaccess.dao.TicketUserDao;
import com.epam.training.ticketservice.dataaccess.converter.Seat;
import com.epam.training.ticketservice.dataaccess.converter.SeatList;
import com.epam.training.ticketservice.dataaccess.dao.BookDao;
import com.epam.training.ticketservice.dataaccess.dao.MovieDao;
import com.epam.training.ticketservice.dataaccess.dao.RoomDao;
import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.dataaccess.model.Movie;
import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.exception.InvalidSeatException;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.SeatAlreadyTakenException;
import com.epam.training.ticketservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.StreamSupport;

@Repository
public class JpaBookRepository implements BookRepository {
    @Autowired
    private BookDao bookDao;

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private RoomDao roomDao;

    @Autowired
    private TicketUserDao ticketUserRepository;

    @Override
    public Book bookSeat(String movie, String room, String startdate, String seats)
        throws MovieNotFoundException, RoomNotFoundException, InvalidSeatException, SeatAlreadyTakenException {
        Optional<Movie> findedMovie = movieDao.findByTitle(movie);
        if (!findedMovie.isPresent()) {
            throw new MovieNotFoundException();
        }
        Optional<Room> findedRoom = roomDao.findByName(room);
        if (!findedRoom.isPresent()) {
            throw new RoomNotFoundException();
        }

        LocalDateTime localTime = LocalDateTime.parse(startdate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        ZonedDateTime time = localTime.atZone(ZoneId.systemDefault());

        SeatList seatList = SeatList.to(seats);

        for (Seat s : seatList) {
            if (s.getRow() < 0 || s.getColumn() < 0 || s.getRow() > findedRoom.get().getRows()
                || s.getColumn() > findedRoom.get().getColumns()) {
                throw new InvalidSeatException("Seat " + s.toString() + " does not exist in this room");
            }
        }

        Optional<List<Book>> bookings = bookDao.findBooksByRoom(findedRoom.get());
        if (bookings.isPresent()) {
            AtomicReference<Seat> taken = new AtomicReference();
            Optional<Book> findedSeat =
                StreamSupport.stream(bookings.get().spliterator(), false).filter(
                    entity -> {
                        for (Seat s : seatList) {
                            if (entity.getSeats().contains(s)) {
                                taken.set(s);
                                return true;
                            }
                        }
                        return false;
                    }
                ).findAny();
            if (findedSeat.isPresent()) {
                throw new SeatAlreadyTakenException("Seat (" + taken.get().toString() + ") is already taken");
            }
        }

        User user =
            ticketUserRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();

        return bookDao.save(new Book(findedMovie.get(),
            findedRoom.get(),
            time,
            seatList,
            1500 * seatList.size(),
            user));
    }

    @Override
    public List<Book> getBooksByUser(String username) {
        User user = ticketUserRepository.findByUsername(username).get();

        Optional<List<Book>> lst = bookDao.findBooksByUser(user);
        if (lst.isPresent()) {
            return lst.get();
        } else {
            return null;
        }
    }
}
