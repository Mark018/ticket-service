package com.epam.training.ticketservice.dataaccess.dao;

import com.epam.training.ticketservice.dataaccess.model.User;
import com.epam.training.ticketservice.dataaccess.model.Book;
import com.epam.training.ticketservice.dataaccess.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookDao extends JpaRepository<Book, Integer> {
    Optional<List<Book>> findBooksByUser(User user);

    Optional<List<Book>> findBooksByRoom(Room room);
}
