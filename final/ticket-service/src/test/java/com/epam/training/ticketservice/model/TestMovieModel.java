package com.epam.training.ticketservice.model;

import com.epam.training.ticketservice.dataaccess.model.Movie;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class TestMovieModel {

    @Test
    public void testSetAndGetId() {
        // Given
        //Movie movie = Mockito.mock(Movie.class);
        Movie movie = new Movie();

        // When
        //Mockito.when(movie.getId()).thenReturn(1);
        movie.setId(1);

        // Then
        Assertions.assertEquals(1, movie.getId());
    }

    @Test
    public void testSetAndGetTitle() {
        // Given
        Movie movie = new Movie();

        // When
        movie.setTitle("title");

        // Then
        Assertions.assertEquals("title", movie.getTitle());
    }

    @Test
    public void testSetAndGetGenre() {
        // Given
        Movie movie = new Movie();

        // When
        movie.setGenre("genre");

        // Then
        Assertions.assertEquals("genre", movie.getGenre());
    }

    @Test
    public void testSetAndGetDuration() {
        // Given
        Movie movie = new Movie();

        // When
        movie.setDuration(1);

        // Then
        Assertions.assertEquals(1, movie.getDuration());
    }

    @Test
    public void testMovieFullArgumentConstructor() {
        // Given
        Movie movie = new Movie("title", "genre", 1);

        // Then / When
        Assertions.assertEquals("title", movie.getTitle());
        Assertions.assertEquals("genre", movie.getGenre());
        Assertions.assertEquals(1, movie.getDuration());
    }

    @Test
    public void testMovieHashCode() {
        // Given
        Movie movie1 = new Movie("title", "genre", 1);
        Movie movie2 = new Movie("new title", "genre", 1);

        // When
        movie2.setTitle(movie1.getTitle());

        // Then
        Assertions.assertEquals(movie1.hashCode(), movie2.hashCode());
    }

    @Test
    public void testMovieHashCodeIsEqual() {
        // Given
        Movie movie1 = new Movie("title", "genre", 1);
        Movie movie2 = new Movie("new title", "genre", 1);

        // When
        movie2.setTitle(movie1.getTitle());

        // Then
        Assertions.assertTrue(movie1.equals(movie2));
    }

    @Test
    public void testMovieHashCodeIsNotEqual() {
        // Given
        Movie movie1 = new Movie("title", "genre", 1);
        Movie movie2 = new Movie("new title", "genre", 1);

        // Then / When
        Assertions.assertFalse(movie1.equals(movie2));
    }

    @Test
    public void testMovieHashCodeIsSame() {
        // Given
        Movie movie = new Movie("title", "genre", 1);

        // Then / When
        Assertions.assertTrue(movie.equals(movie));
    }

    @Test
    public void testMovieHashCodeIsNull() {
        // Given
        Movie movie = new Movie("title", "genre", 1);

        // Then / When
        Assertions.assertFalse(movie.equals(null));
    }

    @Test
    public void testMovieToString() {
        // Given
        Movie movie = new Movie("title", "genre", 1);

        // Then / When
        Assertions.assertEquals("title (genre, 1 minutes)", movie.toString());
    }
}
