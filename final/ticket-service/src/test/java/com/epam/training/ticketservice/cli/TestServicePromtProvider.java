package com.epam.training.ticketservice.cli;

import org.jline.utils.AttributedString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TestServicePromtProvider {
    @InjectMocks
    private ServicePromtProvider servicePromtProvider;

    @Test
    public void testServicePromtProviderShouldReturn() {
        // Given
        AttributedString excepted = new AttributedString("Ticket service>");

        // Then
        Assertions.assertEquals(excepted, servicePromtProvider.getPrompt());
    }
}
