package com.epam.training.ticketservice.model;

import com.epam.training.ticketservice.dataaccess.model.Room;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestRoomModel {

    @Test
    public void testSetAndGetId() {
        // Given
        Room room = new Room();

        // When
        room.setId(1);

        // Then
        Assertions.assertEquals(1, room.getId());
    }

    @Test
    public void testSetAndGetName() {
        // Given
        Room room = new Room();

        // When
        room.setName("name");

        // Then
        Assertions.assertEquals("name", room.getName());
    }

    @Test
    public void testSetAndGetColumns() {
        // Given
        Room room = new Room();

        // When
        room.setColumns(1);

        // Then
        Assertions.assertEquals(1, room.getColumns());
    }

    @Test
    public void testSetAndGetRows() {
        // Given
        Room room = new Room();

        // When
        room.setRows(1);

        // Then
        Assertions.assertEquals(1, room.getRows());
    }

    @Test
    public void testRoomFullArgumentConstructor() {
        // Given
        Room room = new Room("name", 1, 1);

        // Then / When
        Assertions.assertEquals("name", room.getName());
        Assertions.assertEquals(1, room.getRows());
        Assertions.assertEquals(1, room.getColumns());
    }

    @Test
    public void testRoomHashCode() {
        // Given
        Room room1 = new Room("name", 1, 1);
        Room room2 = new Room("new name", 1, 1);

        // When
        room2.setName(room1.getName());

        // Then
        Assertions.assertEquals(room1.hashCode(), room2.hashCode());
    }

    @Test
    public void testRoomHashCodeIsEqual() {
        // Given
        Room room1 = new Room("name", 1, 1);
        Room room2 = new Room("new name", 1, 1);

        // When
        room2.setName(room1.getName());

        // Then
        Assertions.assertTrue(room1.equals(room2));
    }

    @Test
    public void testRoomHashCodeIsNotEqual() {
        // Given
        Room room1 = new Room("name", 1, 1);
        Room room2 = new Room("new name", 1, 1);

        // Then / When
        Assertions.assertFalse(room1.equals(room2));
    }

    @Test
    public void testRoomHashCodeIsSame() {
        // Given
        Room room = new Room("name", 1, 1);

        // Then / When
        Assertions.assertTrue(room.equals(room));
    }

    @Test
    public void testRoomHashCodeIsNull() {
        // Given
        Room room = new Room("name", 1, 1);

        // Then / When
        Assertions.assertFalse(room.equals(null));
    }

    @Test
    public void testRoomToString() {
        // Given
        Room room = new Room("name", 1, 1);

        // Then / When
        Assertions.assertEquals("Room name with 1 seats, 1 rows and 1 columns", room.toString());
    }
}
