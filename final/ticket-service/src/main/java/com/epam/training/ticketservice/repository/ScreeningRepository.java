package com.epam.training.ticketservice.repository;

import com.epam.training.ticketservice.dataaccess.model.Screening;
import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningOverlapException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScreeningRepository {
    void addScreening(String movie, String room, String startdate)
        throws MovieNotFoundException, RoomNotFoundException, ScreeningOverlapException;

    void deleteScreening(String movie, String room, String startdate)
        throws MovieNotFoundException, RoomNotFoundException, ScreeningNotFoundException;

    List<Screening> getScreenings();
}
