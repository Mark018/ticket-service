package com.epam.training.ticketservice.service;

import com.epam.training.ticketservice.exception.MovieNotFoundException;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningNotFoundException;
import com.epam.training.ticketservice.exception.ScreeningOverlapException;
import com.epam.training.ticketservice.repository.ScreeningRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestScreeningService {

    @Mock
    private ScreeningRepository screeningRepository;

    @InjectMocks
    private ScreeningService screeningService;

    @Test
    public void testAddScreeningShouldInvokeTheRepositoryAddScreening()
        throws RoomNotFoundException, MovieNotFoundException, ScreeningOverlapException {
        // Given
        String movie = "movie";
        String room = "room";
        String date = "date";

        // When
        screeningService.addScreening(movie, room, date);

        // Then
        verify(screeningRepository).addScreening(movie, room, date);
        Mockito.verifyNoMoreInteractions(screeningRepository);
    }

    @Test
    public void testDeleteScreeningShouldInvokeTheDeleteScreening()
        throws RoomNotFoundException, MovieNotFoundException, ScreeningNotFoundException {
        // Given
        String movie = "movie";
        String room = "room";
        String date = "date";

        // When
        screeningService.deleteScreening(movie, room, date);

        // Then
        verify(screeningRepository).deleteScreening(movie, room, date);
        Mockito.verifyNoMoreInteractions(screeningRepository);
    }

    @Test
    public void testGetScreeningsShouldInvokeTheRepositoryGetScreenings() {
        // When
        screeningService.getScreenings();

        // Then
        verify(screeningRepository).getScreenings();
        Mockito.verifyNoMoreInteractions(screeningRepository);
    }
}
