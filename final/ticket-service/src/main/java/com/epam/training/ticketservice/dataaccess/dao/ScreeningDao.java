package com.epam.training.ticketservice.dataaccess.dao;

import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.dataaccess.model.Screening;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ScreeningDao extends JpaRepository<Screening, Integer> {
    Optional<List<Screening>> findByRoom(Room room);
}
