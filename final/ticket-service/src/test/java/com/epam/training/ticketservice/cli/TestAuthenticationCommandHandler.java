package com.epam.training.ticketservice.cli;

import com.epam.training.ticketservice.cli.handler.AuthenticationCommandHandler;
import com.epam.training.ticketservice.repository.BookRepository;
import com.epam.training.ticketservice.service.TicketUserDetailsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TestAuthenticationCommandHandler {
    @InjectMocks
    private AuthenticationCommandHandler authenticationCommandHandler;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private BookRepository bookRepository;

    @Mock
    private TicketUserDetailsService ticketUserDetailsService;

    @Test
    public void testSignInToPrivilegedAccountShouldEnterTheAdminAccount() {
        // Given
        String username = "admin";
        String password = "admin";
        Authentication req = new UsernamePasswordAuthenticationToken(username, password);

        // When
        when(authenticationManager.authenticate(req)).thenReturn(req);
        authenticationCommandHandler.singIn(username, password);

        // Then
        verify(authenticationManager).authenticate(req);
        verifyNoMoreInteractions(authenticationManager);
    }

    @Test
    public void testDescribeAccountShouldReturnYouAreNotSignedInWhenYouAreNotSignedIn() {
        // When
        SecurityContextHolder.clearContext();

        // Then
        Assertions.assertEquals("You are not signed in", authenticationCommandHandler.describeAccount());
    }

    @Test
    public void testDescribeAccountShouldYouAreSignInToAdminAccount() {
        // Given
        String username = "admin";
        String password = "admin";
        SimpleGrantedAuthority adminRole = new SimpleGrantedAuthority("ADMIN");
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        authorities.add(adminRole);
        Authentication req = new UsernamePasswordAuthenticationToken(username, password, authorities);

        // When
        SecurityContextHolder.getContext().setAuthentication(req);

        // Then
        Assertions.assertEquals(String.format("Signed in with privileged account '%s'", username),
            authenticationCommandHandler.describeAccount());
    }

    @Test
    public void testDescribeAccountShouldYouAreSignInToUserAccount() {
        // Given
        String username = "admin";
        String password = "admin";
        Authentication req = new UsernamePasswordAuthenticationToken(username, password);

        // When
        when(bookRepository.getBooksByUser(username)).thenReturn(null);
        SecurityContextHolder.getContext().setAuthentication(req);

        // Then
        Assertions.assertEquals(String.format("Signed in with account '%s'"
                + "\nYou have not booked any tickets yet", username),
            authenticationCommandHandler.describeAccount());
    }

    @Test
    public void testSignUpShouldReturnUserIsAlreadyExists() {
        // Given
        String username = "admin";
        String password = "admin";

        // When
        when(ticketUserDetailsService.isUserExists(username)).thenReturn(true);

        // Then
        Assertions.assertEquals("User is already exists!", authenticationCommandHandler.signUp(username, password));
    }

    @Test
    public void testSignUpShouldInvokeTheServiceAddUser() {
        // Given
        String username = "admin";
        String password = "admin";

        // When
        when(ticketUserDetailsService.isUserExists(username)).thenReturn(false);
        authenticationCommandHandler.signUp(username, password);

        // Then
        verify(ticketUserDetailsService).addUser(username, password);
    }

    @Test
    public void testSignInPersonalShouldSuccessfullySignIn() {
        // Given
        String username = "user";
        String password = "user";
        Authentication req = new UsernamePasswordAuthenticationToken(username, password);

        // When
        when(authenticationManager.authenticate(req)).thenReturn(req);

        // Then
        Assertions.assertEquals(null, authenticationCommandHandler.singInToPersonal(username, password));
    }

    @Test
    public void testSignInPersonalShouldReturnUseSignInPrivilegedCommand() {
        // Given
        String username = "user";
        String password = "user";
        SimpleGrantedAuthority adminRole = new SimpleGrantedAuthority("ADMIN");
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        authorities.add(adminRole);
        Authentication req = new UsernamePasswordAuthenticationToken(username, password);
        Authentication res = new UsernamePasswordAuthenticationToken(username, password, authorities);

        // When
        when(authenticationManager.authenticate(req)).thenReturn(res);

        // Then
        Assertions.assertEquals("Please use the 'sign in privileged' command.",
            authenticationCommandHandler.singInToPersonal(username, password));
    }
}
