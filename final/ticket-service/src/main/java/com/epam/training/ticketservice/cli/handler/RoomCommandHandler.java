package com.epam.training.ticketservice.cli.handler;

import com.epam.training.ticketservice.dataaccess.model.Room;
import com.epam.training.ticketservice.exception.RoomNotFoundException;
import com.epam.training.ticketservice.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

import java.util.List;

@ShellComponent
public class RoomCommandHandler {
    @Autowired
    private RoomService roomService;

    @ShellMethod(value = "Create new room", key = "create room")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public void createRoom(String name, int rows, int columns) {
        roomService.addRoom(name, rows, columns);
    }

    @ShellMethod(value = "Update a existing room", key = "update room")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public String updateRoom(String name, int rows, int columns) {
        try {
            roomService.updateRoom(name, rows, columns);
        } catch (RoomNotFoundException e) {
            return "The specified room not exists";
        }
        return null;
    }

    @ShellMethod(value = "Delete a existing room", key = "delete room")
    @ShellMethodAvailability("isUserHasAdminPrivileges")
    public String deleteRoom(String title) {
        try {
            roomService.deleteRoom(title);
        } catch (RoomNotFoundException e) {
            return "The specified room not exists";
        }
        return null;
    }

    @ShellMethod(value = "List all rooms", key = "list rooms")
    public String listRooms() {
        List<Room> roomList = roomService.getRooms();
        if (roomList.size() == 0) {
            return "There are no rooms at the moment";
        } else {
            roomList.forEach(System.out::println);
        }
        return null;
    }
}
