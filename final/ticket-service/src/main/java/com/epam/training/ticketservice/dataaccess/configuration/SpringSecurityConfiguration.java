package com.epam.training.ticketservice.dataaccess.configuration;

import com.epam.training.ticketservice.dataaccess.dao.TicketUserDao;
import com.epam.training.ticketservice.service.TicketUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collections;

@Configuration
public class SpringSecurityConfiguration {

    @Bean
    public UserDetailsService ticketUserDetailsService(TicketUserDao ticketUserRepository) {
        return new TicketUserDetailsService(ticketUserRepository);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(UserDetailsService userDetailsService) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());

        return new ProviderManager(Collections.singletonList(authenticationProvider));
    }

}
